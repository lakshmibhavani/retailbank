import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/classes/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  user: User;

  accountNumber;
  constructor(private userService: UserService, private router: Location) {

  }
  ngOnInit() {

  }
  navBack() {
    this.router.back();
  }
  Search() {
    this.userService.getUserByAccountNumber(this.accountNumber).subscribe(res => {
      this.user = res;
    });
  }

}
