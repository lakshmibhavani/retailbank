import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { User } from 'src/app/classes/user';
import { CurrentUserService } from 'src/app/services/current-user.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  user: User = new User();

  constructor(currentUser: CurrentUserService, private router: Location) {
    console.log(currentUser.getUserData());
    this.user = currentUser.getUserData();
  }

  ngOnInit(): void {
  }
  navBack() {
    this.router.back();
  }

}
