import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/classes/user';
import { Location } from '@angular/common';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {

  user: User = new User();
  user1: User = new User();
  constructor(private router: Router, private us: UserService, private router1: Location) { }

  ngOnInit(): void {

  }
  onUpdate() {
    console.log("into update");
    console.log(this.user.id + "," + this.user.userName + "," + this.user.emailId + "," + this.user.phoneNumber + "," + this.user.accountNumber);
    let id = localStorage.getItem("id");
    this.us.getUser(+id).subscribe(data => {
      this.user1 = data;
      this.user.id = this.user1.id;
      this.user.accountNumber = this.user.accountNumber;
      this.user.balance = this.user.balance;
      this.user.isactive = true;
      this.user.type = this.user.type;
      this.user.userName = this.user1.userName;
      this.user.password = this.user1.password;
      this.user.emailId = this.user1.emailId;
      this.user.phoneNumber = this.user1.phoneNumber;
      this.user.address = this.user1.address;
      this.us.update(this.user.id, this.user).subscribe(data => { console.log(data); }, error => console.log(error));
      this.router.navigate(['admin']);

    })


  }
  navBack() {
    this.router1.back();
  }


}
