import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/classes/user';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  submitted = false;
  users: User[] = [];
  user: User = new User();
  constructor(private formBuilder: FormBuilder, public us: UserService, public router: Router) { }

  ngOnInit() { }
  registerForm = new FormGroup({
    userName: new FormControl('', Validators.required),
    phoneNumber: new FormControl('', [Validators.required, Validators.minLength(10)]),
    emailId: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    confirmPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
    address: new FormControl('', [Validators.required])
  });

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    else {

      let pass = this.registerForm.get('password').value
      let confirmPass = this.registerForm.get('confirmPassword').value;
      console.log(pass);
      console.log(confirmPass);
      if (pass == confirmPass) {
        this.user.emailId = this.registerForm.get('emailId').value;
        this.user.userName = this.registerForm.get('userName').value;
        this.user.phoneNumber = this.registerForm.get('phoneNumber').value;
        this.user.password = this.registerForm.get('password').value;
        this.user.address = this.registerForm.get('address').value;
        this.user.isactive = false;
        this.user.type = '';
        this.user.accountNumber = '';
        this.user.balance = 0;
        console.log(this.user);
        this.us.createUsers(this.user).subscribe();
        this.registerForm.reset();
        alert("Registration done succefully now you can login");
        this.router.navigate(['login']);
      }
      else {
        alert("Password and confirm password not match.");
      }

    }
  }
}