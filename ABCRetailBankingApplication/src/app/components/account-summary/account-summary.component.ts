import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Transaction } from 'src/app/classes/transaction';
import { User } from 'src/app/classes/user';
import { TransactionService } from 'src/app/services/transaction.service';
import { CurrentUserService } from 'src/app/services/current-user.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-account-summary',
  templateUrl: './account-summary.component.html',
  styleUrls: ['./account-summary.component.css']
})
export class AccountSummaryComponent implements OnInit {

  transactions: Transaction[] = [];
  transactions1: Transaction[] = [];
  transaction: Transaction = new Transaction();
  transaction1: Transaction = new Transaction();
  user: User = new User();

  toAccountNumber;
  fileName = "transactions.xlsx";
  constructor(public ts: TransactionService, currentUser: CurrentUserService, private router: Location) {
    this.user = currentUser.getUserData();
    currentUser.setUserData(this.user);
    this.transaction1.fromAccountNumber = this.user.accountNumber;
  }

  ngOnInit() {
    this.ts.getTransactionByAccNumber(this.user.accountNumber).subscribe(res => {
      this.transactions = res;
    });
  }
  navBack() {
    this.router.back();
  }


  exportexcel(): void {
    let element = document.getElementById('fileName');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);

  }

}
