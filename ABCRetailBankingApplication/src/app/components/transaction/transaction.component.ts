import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { User } from 'src/app/classes/user';
import { Transaction } from 'src/app/classes/transaction';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { TransactionService } from 'src/app/services/transaction.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {

  users: User[] = [];
  users1: User[] = [];
  user: User = new User();
  user1: User = new User();
  submitted = false;
  fromaccountnumber;
  sender;
  toaccountnumber;
  receiver;
  amount;
  date = new Date();
  transaction: Transaction = new Transaction();
  constructor(private formBuilder: FormBuilder, public us: UserService, public ts: TransactionService, currentUser: CurrentUserService
    , private router: Location) {

    this.user = currentUser.getUserData();
    currentUser.setUserData(this.user);
  }

  ngOnInit(): void {
  }
  transfer = new FormGroup({
    toaccountnumber: new FormControl('', Validators.required),
    transactionamount: new FormControl('', Validators.required),
    transactionControl: new FormControl('', Validators.required)
  });
  get f() { return this.transfer.controls; }
  isDebitSelected: boolean;
  selectInput(event) {
    let selected = event.target.value;
    if (selected == "debit") {
      this.isDebitSelected = true;
    } else {
      this.isDebitSelected = false;
    }
  }
  onSubmit() {
    if (this.isDebitSelected == true) {

      this.us.getUsers().subscribe(res => {
        res.forEach(res => {
          this.users.push(res);
          console.log(this.users);
        })

        let user2 = this.users.filter(res => {
          return res.accountNumber == this.transfer.get('toaccountnumber').value
        });
        console.log(user2);
        if (user2[0].accountNumber != this.user.accountNumber) {
          if (this.user.balance >= this.transfer.get('transactionamount').value) {
            user2[0].balance = user2[0].balance + this.transfer.get('transactionamount').value;
            this.user.balance = this.user.balance - this.transfer.get('transactionamount').value;
            this.us.update(this.user.id, this.user).subscribe();
            this.us.update(user2[0].id, user2[0]).subscribe();
            this.transaction.fromAccountNumber = this.user.accountNumber;
            this.transaction.toAccountNumber = user2[0].accountNumber;
            this.transaction.transactionAmmount = this.transfer.get('transactionamount').value;
            this.transaction.transactiondate = this.date;
            this.transaction.balance = this.user.balance - this.transfer.get('transactionamount').value;
            this.transaction.message = "Transaction Successfull";
            this.transaction.transactiontype = "debit";
            this.ts.createTransaction(this.transaction).subscribe();
            alert('Transaction Successfull..\n Amount credited  to ' + user2[0].userName + ' with account number ' + this.transaction.toAccountNumber);
          }
        }
        else {
          alert('self transaction can not be done through debit');
        }

      });
    }
    else {
      this.us.getUsers().subscribe(res => {
        res.forEach(res => {
          this.users.push(res);

        })
        if (this.user.balance >= this.transfer.get('transactionamount').value) {
          this.user.balance = this.user.balance + this.transfer.get('transactionamount').value;
          this.us.update(this.user.id, this.user).subscribe();
          this.transaction.fromAccountNumber = this.user.accountNumber;
          this.transaction.toAccountNumber = this.user.accountNumber;
          this.transaction.transactionAmmount = this.transfer.get('transactionamount').value;
          this.transaction.transactiondate = this.date;
          this.transaction.balance = this.user.balance + this.transfer.get('transactionamount').value;
          this.transaction.message = "Transaction Successfull";
          this.transaction.transactiontype = "credit";
          this.ts.createTransaction(this.transaction).subscribe();
          alert('Credit Successfull');
        } else {
          alert('You cannot credit more than available amount');
        }

      });
    }

  }
  navBack() {
    this.router.back();
  }

}
