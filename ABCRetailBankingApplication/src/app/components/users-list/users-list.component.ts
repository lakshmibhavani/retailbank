import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/classes/user';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  user: User = new User();

  constructor(currentUser: CurrentUserService, private router: Location) {
    console.log(currentUser.getUserData());
    this.user = currentUser.getUserData();
  }

  ngOnInit(): void {
  }
  navBack() {
    this.router.back();
  }

}
