import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/classes/user';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { CurrentUserService } from 'src/app/services/current-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  users: User[] = [];
  user: User = new User();
  submitted = false;
  constructor(private formBuilder: FormBuilder, public us: UserService, private router: Router, private currentUser: CurrentUserService) { }

  ngOnInit(): void {
  }
  loginForm = new FormGroup({
    userName: new FormControl('', Validators.required),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });
  get f() { return this.loginForm.controls; }
  /* onLoginPress(){
   
  } */
  onSubmit() {
    this.us.getUsers().subscribe((response) => {
      this.users = response;
      console.log(this.users)
      this.submitted = true;
      let usrname = this.loginForm.get('userName').value;
      let pwd = this.loginForm.get('password').value;
      for (this.user of this.users) {

        if ((usrname == this.user.userName) && (pwd == this.user.password)) {
          console.log(this.user.userName);
          console.log(this.user.password);
          if (this.user.type == 'user') {
            this.currentUser.setUserData(this.user);
            this.user.isactive = true;
            this.router.navigate(['user']);
          }
          if (this.user.type == 'admin') {
            this.currentUser.setUserData(this.user);
            this.user.isactive = true;
            this.router.navigate(['admin']);
          }
          if ((this.user.type != 'user') && (this.user.type != 'admin')) {
            alert("admin will verify please wait");
          }
        }

      }
    });
    if (this.loginForm.invalid) {

      alert("please provide correct login credentials");

      return;
    }
  }
  register() {
    this.router.navigate(['register']);
  }

}
