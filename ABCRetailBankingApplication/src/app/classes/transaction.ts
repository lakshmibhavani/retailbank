export class Transaction {
    id:number;
    toAccountNumber:string;
    fromAccountNumber:string;
    transactiondate:Date;
    message:string;
    transactiontype:string;
    balance:number;
    transactionAmmount:number;
    status:string;
}
