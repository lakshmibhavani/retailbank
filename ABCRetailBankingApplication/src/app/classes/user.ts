
export class User {
    id: number;
    userName:string;
    password:string;
    confirmPassword:string;
    emailId:string;
    phoneNumber:number;
    accountNumber:string;
    type:string;
    balance:number;
    isactive: boolean;
    address:string;
}
