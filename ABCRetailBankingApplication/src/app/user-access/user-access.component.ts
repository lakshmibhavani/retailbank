import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from '../services/current-user.service';
import { User } from '../classes/user';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-access',
  templateUrl: './user-access.component.html',
  styleUrls: ['./user-access.component.css']
})
export class UserAccessComponent implements OnInit {

  user:User=new User();

  constructor( private currentUser:CurrentUserService,private router:Router,private us:UserService) { 
    console.log(currentUser.getUserData());
    this.user=currentUser.getUserData();
    currentUser.setUserData(this.user);
  }

  ngOnInit(): void {
  }
  
  logout(){
    this.currentUser.setUserData(null);
    this.user.isactive=false;
    console.log(this.user);
    this.us.update(this.user.id,this.user).subscribe();
    this.router.navigate(['login']);
  }


}
