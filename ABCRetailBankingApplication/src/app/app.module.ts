import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateAccountComponent } from './components/create-account/create-account.component';
import { UserService } from './services/user.service';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { TransactionComponent } from './components/transaction/transaction.component';
import { UserAccessComponent } from './user-access/user-access.component';
import { AdminAccessComponent } from './admin-access/admin-access.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { AccountSummaryComponent } from './components/account-summary/account-summary.component';
import { AllTransactionsComponent } from './components/all-transactions/all-transactions.component';
import { MyProfileComponent } from './components/my-profile/my-profile.component';
import { SearchComponent } from './components/search/search.component';
import { HomeComponent } from './components/home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    CreateAccountComponent,
    RegisterComponent,
    LoginComponent,
    TransactionComponent,
    UserAccessComponent,
    AdminAccessComponent,
    UsersListComponent,
    AccountSummaryComponent,
    AllTransactionsComponent,
    MyProfileComponent,
    SearchComponent,
    HomeComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
   
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
