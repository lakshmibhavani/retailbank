import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AdminAccessComponent } from './admin-access/admin-access.component';
import { UserAccessComponent } from './user-access/user-access.component';
import { CreateAccountComponent } from './components/create-account/create-account.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { TransactionComponent } from './components/transaction/transaction.component';
import { AccountSummaryComponent } from './components/account-summary/account-summary.component';
import { AllTransactionsComponent } from './components/all-transactions/all-transactions.component';
import { MyProfileComponent } from './components/my-profile/my-profile.component';
import { SearchComponent } from './components/search/search.component';

import { HomeComponent } from './components/home/home.component';

const routes: Routes = [{path:'login',component:LoginComponent},
{path:'register',component:RegisterComponent},
{path:'user',component:UserAccessComponent},
  {path:'admin',component:AdminAccessComponent},
  {path:'edit',component:CreateAccountComponent},
  {path:'updateduser',component:UsersListComponent},
  {path:'admin/transfer',component:TransactionComponent},
  {path:'admin/history',component:AccountSummaryComponent},
  {path:'admin/alltransactions',component:AllTransactionsComponent},
  {path:'admin/myprofile',component:MyProfileComponent},
  {path:'admin/search',component:SearchComponent},
  {path:'user/transfer',component:TransactionComponent},
  {path:'user/history',component:AccountSummaryComponent},
 
  {path:'user/myprofile',component:MyProfileComponent},
  {path:'search',component:SearchComponent},
  {path:'',component:HomeComponent}
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
