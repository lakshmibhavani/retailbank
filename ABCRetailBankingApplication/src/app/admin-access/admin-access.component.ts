import { Component, OnInit } from '@angular/core';
import { User } from '../classes/user';
import { CurrentUserService } from '../services/current-user.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-access',
  templateUrl: './admin-access.component.html',
  styleUrls: ['./admin-access.component.css']
})
export class AdminAccessComponent implements OnInit {
  user:User=new User();
  accountNumber:any;
  user1:User=new User();
  user2:User=new User();
  users:any;
  constructor(private currentUser:CurrentUserService,private router:Router,public us:UserService) { 
    console.log(currentUser.getUserData());
    this.user=currentUser.getUserData();
    currentUser.setUserData(this.user);
  }
  
  ngOnInit(): void {
    this.users = this.us.getUsers();
  }
  getuser(){
    this.us.getUsers().subscribe((response)=>{
      this.users=response;
    })
  }
 search(){
  this.getuser();
  this.users=this.users.filter(res=>{
    return res.accountNumber.match(this.accountNumber)
  })
 }
 
  editUser(user: any): void {
   
    localStorage.setItem("id", user.id);
    console.log(user.id);
    this.router.navigate(["edit"]);
  }
  logout(){
    this.currentUser.setUserData(null);
    this.user.isactive=false;
    this.us.update(this.user.id,this.user).subscribe();
    this.router.navigate(["login"]);
  }
  deleteUser(user: any) {
    console.log(user.id);
    this.us.deleteUser(user.id).subscribe(data => {
      console.log(data);
      this.ngOnInit();
    }, error => console.log(error));

  }
}
